//
//  KLMHouseCollectionViewCell.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import UIKit

class KLMHouseCollectionViewCell: UICollectionViewCell {
	
	@IBOutlet weak var identifierLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.layer.shouldRasterize = true
		self.layer.rasterizationScale = UIScreen.mainScreen().scale
		self.layer.borderColor = UIColor.lightGrayColor().CGColor
		self.layer.borderWidth = 1.0 / UIScreen.mainScreen().scale
	}
}
