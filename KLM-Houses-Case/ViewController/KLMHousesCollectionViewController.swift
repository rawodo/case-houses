//
//  KLMHousesCollectionViewController.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import UIKit

private let houseCellReuseIdentifier = "HouseCell"

class KLMHousesCollectionViewController: UICollectionViewController {

	// ====================================================================================================
	// MARK: - View Lifecycle
	// ====================================================================================================

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.registerClass(KLMHouseCollectionViewCell.self, forCellWithReuseIdentifier: houseCellReuseIdentifier)
		self.collectionView!.registerNib(UINib(nibName: "HouseCell", bundle: nil), forCellWithReuseIdentifier: houseCellReuseIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	// ====================================================================================================
    // MARK: - Navigation
	// ====================================================================================================

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }

	// ====================================================================================================
    // MARK: UICollectionViewDataSource
	// ====================================================================================================
	
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		print("collectionView.numberOfItemsInSection called on wrong class")
		exit(-1)
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		return collectionView.dequeueReusableCellWithReuseIdentifier(houseCellReuseIdentifier, forIndexPath: indexPath) as! KLMHouseCollectionViewCell
    }

}
