//
//  KLMRootNavigationController.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import UIKit

class KLMRootNavigationController: UINavigationController {

	@IBAction func dismiss(sender: UIStoryboardSegue) {
		// Empty action, here for unwind segue support.
	}
}
