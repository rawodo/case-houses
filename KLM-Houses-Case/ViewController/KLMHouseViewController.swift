//
//  KLMHouseViewController.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import UIKit

class KLMHouseViewController: KLMViewController {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var houseNameLabel: UILabel!
	@IBOutlet weak var houseAddressLabel: UILabel!
	@IBOutlet weak var identifierLabel: UILabel!
	
	@IBOutlet weak var possessionCountButtonItem: UIBarButtonItem!
	
	@IBOutlet weak var possessionRemoveButton: UIBarButtonItem!
	@IBOutlet weak var possessionAddButton: UIBarButtonItem!
	
	internal var house: KLMHouse?
	
    override func viewDidLoad() {
        super.viewDidLoad()

		if let houseToLoadNow = self.house {
			self.fillWithHouse(houseToLoadNow)
		}
    }

	func fillWithHouse(house: KLMHouse) {
		self.house = house

		if !self.isViewLoaded() {
			return
		}
		
		self.navigationItem.title = house.name
		self.imageView.image = house.image
		self.houseNameLabel.text = house.name
		self.houseAddressLabel.text = house.address
		
		let formatter = NSNumberFormatter()
		self.identifierLabel.text = formatter.stringFromNumber(house.identifier!)

		self.updatePossessionCountLabel()
	}
	
	func updatePossessionCountLabel() {
		let formatter = NSNumberFormatter()
		self.possessionCountButtonItem.title = formatter.stringFromNumber(self.house!.inPossessionCount!)
		
		self.possessionCountButtonItem.enabled = false
		self.possessionRemoveButton.enabled = self.house!.inPossessionCount!.intValue != 0
	}
	
	@IBAction func removePossession(sender: AnyObject) {
		if self.house?.inPossessionCount!.intValue > 0 {
			self.house?.inPossessionCount = NSNumber(int: (self.house?.inPossessionCount?.intValue)! - 1)
			KLMHousesManager.sharedInstance.saveContext()
			self.updatePossessionCountLabel()
		}
	}
	
	@IBAction func addPossession(sender: AnyObject) {
		self.house?.inPossessionCount = NSNumber(int: (self.house?.inPossessionCount?.intValue)! + 1)
		KLMHousesManager.sharedInstance.saveContext()
		self.updatePossessionCountLabel()
	}
	
	@IBAction func toggleFavorite(sender: AnyObject) {
	}
	
	@IBAction func toggleInformation(sender: AnyObject) {
	}
	
}
