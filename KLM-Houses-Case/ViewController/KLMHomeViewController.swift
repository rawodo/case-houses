//
//  KLMHomeViewController.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import UIKit
import Darwin

enum HousesCollectionViewFilter {
	case All
	case Missing
	case Collected
	case Duplicate
}

class KLMHomeViewController: KLMViewController, UICollectionViewDataSource, UICollectionViewDelegate {

	private var activeFilter = HousesCollectionViewFilter.All
	
	private var housesCollectionViewController : KLMHousesCollectionViewController? {
		willSet {
			newValue?.collectionView?.dataSource = self
			newValue?.collectionView?.delegate = self
		}
		didSet {
			if self.isViewLoaded() {
				// Force a reload of the view was already loaded
				housesCollectionViewController?.collectionView?.reloadData()
			}
		}
	}
	
	private var houses : [KLMHouse] {
		get {
			switch self.activeFilter {
			case .All: return KLMHousesManager.sharedInstance.allHouses()
			case .Missing: return KLMHousesManager.sharedInstance.housesMissing()
			case .Collected: return KLMHousesManager.sharedInstance.housesCollected()
			case .Duplicate: return KLMHousesManager.sharedInstance.housesDuplicate()
			}
		}
	}
	
	private func houseForIndexPath(indexPath: NSIndexPath) -> KLMHouse {
		return houses[indexPath.row]
	}

	@IBAction func didChangeFilterControl(sender: UISegmentedControl) {
		switch sender.selectedSegmentIndex {
		case 0: self.activeFilter = .All
		case 1: self.activeFilter = .Missing
		case 2: self.activeFilter = .Collected
		case 3: self.activeFilter = .Duplicate
		default: break
		}
		
		self.housesCollectionViewController?.collectionView?.reloadData()
	}
	

	// ====================================================================================================
	// MARK: - View Lifecycle
	// ====================================================================================================
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.housesCollectionViewController?.collectionView?.reloadData()
	}
	
	
	// ====================================================================================================
    // MARK: - Navigation
	// ====================================================================================================

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "Embed-HousesCollection" {
			if !(segue.destinationViewController is KLMHousesCollectionViewController) {
				print("Unexpected class when embedding HousesCollection")
				exit(-1)
			}
			housesCollectionViewController = segue.destinationViewController as? KLMHousesCollectionViewController
		}
		
		if segue.identifier == "ShowHouse" && sender is KLMHouse {
			if let detailViewController = segue.destinationViewController as? KLMHouseViewController, senderHouse = sender as? KLMHouse {
				detailViewController.fillWithHouse(senderHouse)
			}
		}
    }
	

	// ====================================================================================================
	// MARK: - Collection View Datasource
	// ====================================================================================================
	
	static let identifierFormatter = NSNumberFormatter()
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = (housesCollectionViewController?.collectionView(collectionView, cellForItemAtIndexPath: indexPath))!
		
		if cell is KLMHouseCollectionViewCell {
			let houseCell = cell as! KLMHouseCollectionViewCell
			let house = self.houseForIndexPath(indexPath)
			
			houseCell.identifierLabel!.text = KLMHomeViewController.identifierFormatter.stringFromNumber(house.identifier!)
			houseCell.imageView!.image = house.image
		}

		return cell
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch section {
		case 0: return self.houses.count
		default: return 0
		}
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		let house = self.houses[indexPath.row]
		self.performSegueWithIdentifier("ShowHouse", sender: house)
	}
}
