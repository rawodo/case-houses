//
//  KLMHouse+CoreDataProperties.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension KLMHouse {

    @NSManaged var identifier: NSNumber?
    @NSManaged var address: String?
    @NSManaged var coordinateLongitude: NSNumber?
    @NSManaged var coordinateLatitude: NSNumber?
    @NSManaged var descriptionEN: String?
    @NSManaged var descriptionNL: String?
    @NSManaged var imageName: String?
    @NSManaged var name: String?
    @NSManaged var inPossessionCount: NSNumber?

}
