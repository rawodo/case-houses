//
//  KLMHouse.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class KLMHouse: NSManagedObject {

	lazy var image: UIImage = {
		if let actualImageName : String = self.imageName {
			if let actualImage = UIImage(named: actualImageName) {
				return actualImage
			}
			else {
				return UIImage()
			}
		}
		else {
			return UIImage()
		}
	}()

}
