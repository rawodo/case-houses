//
//  KLMHousesManager.swift
//  KLM-Houses-Case
//
//  Created by Bart Ramakers on 21/03/16.
//  Copyright © 2016 Rawodo. All rights reserved.
//

import UIKit
import CoreData

class KLMHousesManager: NSObject {
	
	static let sharedInstance = KLMHousesManager()
	
	lazy private var houses: [KLMHouse] = {
		let allHouses = self.getAllHousesFromDatabase()
		return allHouses
	}()
	
	func allHouses() -> [KLMHouse] {
		return self.houses
	}
	
	// Return all houses, filtered where inPossessionCount = 0
	func housesMissing() -> [KLMHouse] {
		return KLMHousesManager.sharedInstance.houses.filter({ (house: KLMHouse) -> Bool in
			return house.inPossessionCount!.intValue == 0
		})
	}
	
	// Return all houses, filtered where inPossessionCount > 0
	func housesCollected() -> [KLMHouse] {
		return KLMHousesManager.sharedInstance.houses.filter({ (house: KLMHouse) -> Bool in
			return house.inPossessionCount!.intValue > 0
		})
	}
	
	// Return all houses, filtered where inPossessionCount > 1
	func housesDuplicate() -> [KLMHouse] {
		return KLMHousesManager.sharedInstance.houses.filter({ (house: KLMHouse) -> Bool in
			return house.inPossessionCount!.intValue > 1
		})
	}

	private func getAllHousesFromDatabase() -> [KLMHouse] {
		var allHouses: [KLMHouse] = self.fetchAllHousesFromDatabase()
		
		if allHouses.count == 0 {
			if (KLMHousesManager.didTryLoadingHousesFromPropertyList) {
				print("Failed to load houses from plist")
				return []
			} else {
				// No database import done yet, let's import houses from the plist into the personal database
				createPersonalDatabaseFromPropertyList()
				allHouses = self.fetchAllHousesFromDatabase()
			}
		}
		
		return allHouses
	}
	
	private func fetchAllHousesFromDatabase() -> [KLMHouse] {
		let request = NSFetchRequest(entityName: "House")
		request.sortDescriptors = [NSSortDescriptor(key: "identifier", ascending: true)]
		
		do {
			return try (managedObjectContext.executeFetchRequest(request) as? [KLMHouse])!
		} catch {
			print("Failed to fetch houses.")
			return []
		}
	}

	static var didTryLoadingHousesFromPropertyList = false
	
	private func createPersonalDatabaseFromPropertyList() -> Bool {
		var wasSuccessful = false
		KLMHousesManager.didTryLoadingHousesFromPropertyList = true
		
		var plistDict = NSDictionary()
		if let path = NSBundle.mainBundle().pathForResource("Miniatures", ofType: "plist") {
			plistDict = NSDictionary(contentsOfFile: path)!
		}
		
		let miniatures = plistDict["miniatures"] as! Array<NSDictionary>
		
		for miniature in miniatures {
			let house = NSEntityDescription.insertNewObjectForEntityForName("House", inManagedObjectContext: managedObjectContext) as! KLMHouse
			
			house.identifier = self.getNSNumberFromAnyObject(miniature["number"])
			house.coordinateLatitude = self.getNSNumberFromAnyObject(miniature["latitude"])
			house.coordinateLongitude = self.getNSNumberFromAnyObject(miniature["longitude"])
			house.name = miniature["name"] as! String!
			house.address = miniature["address"] as! String!
			house.descriptionEN = miniature["descriptionEN"] as! String!
			house.descriptionNL = miniature["descriptionNL"] as! String!
			house.imageName = miniature["image"] as! String!
			
			if house.identifier?.integerValue > 0 {
				self.managedObjectContext.insertObject(house)
			} else {
				print("Found house without identifier number: \(house) in dictionary \(miniature)")
			}
			
			wasSuccessful = true
		}
		
		self.saveContext()
		return wasSuccessful
	}
	
	internal func getNSNumberFromAnyObject(input: AnyObject?) -> NSNumber {
		if input is NSNumber {
			return input as! NSNumber!
		} else if input is String {
			if let identifierString = input as! String! {
				return NSNumber(double: Double(identifierString)!)
			} else {
				return NSNumber(int: 0)
			}
		} else {
			return NSNumber(int: 0)
		}
	}
	

	// ====================================================================================================
	// MARK: - Core Data stack
	// ====================================================================================================
	
	lazy var applicationDocumentsDirectory: NSURL = {
		let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
		return urls[urls.count-1]
	}()
	
	lazy var managedObjectModel: NSManagedObjectModel = {
		let modelURL = NSBundle.mainBundle().URLForResource("KLM_Houses_Case", withExtension: "momd")!
		return NSManagedObjectModel(contentsOfURL: modelURL)!
	}()
	
	var persistentStoreExists : Bool {
		return NSFileManager.defaultManager().fileExistsAtPath(self.persistentStoreUrl.path!)
	}
	
	lazy var persistentStoreUrl : NSURL = {
		return self.applicationDocumentsDirectory.URLByAppendingPathComponent("KLMHouses.sqlite")
	}()
	
	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
		let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
		var failureReason = "There was an error creating or loading the application's saved data."
		do {
			try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: self.persistentStoreUrl, options: nil)
		} catch {
			var dict = [String: AnyObject]()
			dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
			dict[NSLocalizedFailureReasonErrorKey] = failureReason
			
			dict[NSUnderlyingErrorKey] = error as NSError
			let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)

			NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
			abort()
		}
		
		return coordinator
	}()
	
	lazy var managedObjectContext: NSManagedObjectContext = {
		let coordinator = self.persistentStoreCoordinator
		var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
		managedObjectContext.persistentStoreCoordinator = coordinator
		return managedObjectContext
	}()
	
	// ====================================================================================================
	// MARK: - Core Data Saving support
	// ====================================================================================================
	
	func saveContext () {
		if managedObjectContext.hasChanges {
			do {
				try managedObjectContext.save()
			} catch {
				let nserror = error as NSError
				NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
				abort()
			}
		}
	}

}
